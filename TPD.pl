#!/usr/bin/perl -w
#use List::Util qw(max);

#
#################################################################################
# Program : TPD.pl															#
# Type    : Perl program                                                        #
# Author  :  Yingtao Bi                                               #
#################################################################################

#################################################################################
                                                                 #
#################################################################################

require 'ctime.pl';    # Should be in your perl library (standard)

sub print_current_time {
        chop( $date_current = `date` );
        print "$date_current\n";
}
########################## new for new seq scan 2
##>/DataRepository/Genome/mm9/chr7.fa.nib:3300270-3301070  for head list
sub find_multiple_bs_double_strand2 {    #  should return a list,. not a file  \@seq_list, \$oldroot
	my (@seq_list)       = @{ $_[0] };
	my (@rcseq_list)       = @{ $_[1] };
	my ($treeroot)       = $_[2];
	my ($length_seqlist) = scalar(@seq_list);
	#my ($boolean) = $_[3];
	my ($cut_score)  = $_[3];
	#my (@bs_list)  = @{ $_[2] };
	#my (@bs_list_st)  = @{ $_[3] };
	#my (@bs_list_score)  = @{ $_[4] };
	

	for ( $i = 0 ; $i < $length_seqlist ; $i++ ) {
		
		
		
		#print OUTBED "$head_list_negative[$i]\n";
		@heads = split(/\:/,$head_list_negative[$i]);
		@tmp=split(/\./,$heads[0]);
		@tmp = split(/\//,$tmp[0]);  # $tmp[4] is the chr and number
		@loca = split(/\-/, $heads[1]);
		
		my ($seq_len) = length( $seq_list[$i] );
		
		my ($max_st)      = 0;
		my ($total_score) = 0;
		my ($max_score) = 0;
		my ($max_strand) = "+";
		for ( $st = 0 ; $st < $seq_len - $site_len + 1 ; ++$st ) {

			## array slice
			##@subseq =  @sq[$st..($st + $site_len)];
			#$subconsensus = substr $seq_list[$i], $max_pos, 1, "";
			$subseq = substr $seq_list[$i], $st, $site_len;
			
			$total_score = compute_score( $treeroot, $subseq );
			if ( $total_score > $max_score ) {
				$max_score  = $total_score;
				$max_subseq = $subseq;
				$max_st     = $st;
			}

		}
		### for the reverse complement seq
		for ( $st = 0 ; $st < $seq_len - $site_len + 1 ; ++$st ) {

			## array slice
			##@subseq =  @sq[$st..($st + $site_len)];
			#$subconsensus = substr $seq_list[$i], $max_pos, 1, "";
			$subseq = substr $rcseq_list[$i], $st, $site_len;
			
			$total_score = compute_score( $treeroot, $subseq );
			if ( $total_score > $max_score ) {
				$max_score  = $total_score;
				$max_subseq = $subseq;
				$max_st     = $st;
				$max_strand = "-";

				
			}

		}
		########### end of the reverse complement
		#print "$i\t$max_subseq\t"."$max_score\n";
		if ( $max_score >= $cut_score ) {
			if ( $max_strand eq "+" ) {
				print OUTBED "$tmp[4]\t";
				$start = $loca[0] + $max_st;
				$temp = $tmp[4].":".$max_subseq.":".$max_score;
				print OUTBED "$start\t",$start+$site_len-1,"\t$temp\t"."0\t"."+\n"; 			
			} else {
			
				print OUTBED "$tmp[4]\t";
				$end = $loca[1] - $max_st-1;
                                $temp = $tmp[4].":".$max_subseq.":".$max_score;
                                print OUTBED $end-$site_len+1,"\t$end\t"."$temp\t"."0\t"."-\n";
			}
		}
		
	}

	#return ( @bs_list, @bs_list_st, @bs_list_score );

}

############################ end new for new seq scan2
########################## new for new seq scan 
##>/DataRepository/Genome/mm9/chr7.fa.nib:3300270-3301070  for head list
sub find_multiple_bs_double_strand {    #  
	my (@seq_list)       = @{ $_[0] };
	my (@rcseq_list)       = @{ $_[1] };
	my ($treeroot)       = $_[2];
	my ($length_seqlist) = scalar(@seq_list);
	
	my ($max_score)  = $_[3];
	
	

	for ( $i = 0 ; $i < $length_seqlist ; $i++ ) {
		
		
		
		print OUTBED "$head_list_negative[$i]\n";
		@heads = split(/\:/,$head_list_negative[$i]);
		@tmp=split(/\./,$heads[0]);
		@tmp = split(/\//,$tmp[0]);  
		@loca = split(/\-/, $heads[1]);
		
		my ($seq_len) = length( $seq_list[$i] );
		
		my ($max_st)      = 0;
		my ($total_score) = 0;
		for ( $st = 0 ; $st < $seq_len - $site_len + 1 ; ++$st ) {

			## array slice
			
			$subseq = substr $seq_list[$i], $st, $site_len;
			
			$total_score = compute_score( $treeroot, $subseq );
			if ( $total_score > $max_score ) {
				
				
				print OUTBED "$tmp[4]\t";
				$start = $loca[0] + $st;
				$temp = $tmp[4].":".$subseq.":".$total_score;
				print OUTBED "$start\t",$start+$site_len-1,"\t$temp\t"."0\t"."+\n"; 
				
			}

		}
		### for the reverse complement seq
		for ( $st = 0 ; $st < $seq_len - $site_len + 1 ; ++$st ) {

			## array slice
			
			$subseq = substr $rcseq_list[$i], $st, $site_len;
			
			$total_score = compute_score( $treeroot, $subseq );
			if ( $total_score > $max_score ) {
				#print "$total_score\n";
				
				print OUTBED "$tmp[4]\t";
				$end = $loca[1] - $st-1;
                                $temp = $tmp[4].":".$subseq.":".$total_score;
                                print OUTBED $end-$site_len+1,"\t$end\t"."$temp\t"."0\t"."-\n";

				
			}

		}
		########### end of the reverse complement
		

		
	}

	

}

############################ end new for new seq scan



sub reverseComplement {     # SUBROUTINE DEFINITION TO

   $tmpSeq = $_[0];         # CREATE THE REVERSE COMPLEMENT

                            # OF A SEQUENCE

   $seqRC = "";

   $strLen = length($tmpSeq);

 

   for($I = ($strLen-1); $I >= 0; $I--) {

       $tmpStr = substr($tmpSeq, $I, 1);

       if($tmpStr eq "a")    { $seqRC .= "t"; }

       elsif($tmpStr eq "c") { $seqRC .= "g"; }

       elsif($tmpStr eq "g") { $seqRC .= "c"; }

       else                  { $seqRC .= "a"; }

   }

   return($seqRC);

}
# fisher_yates_shuffle( \@array ) : generate a random permutation
# of @array in place
sub fisher_yates_shuffle {
	my $array = shift;
	my $i;
	for ( $i = @$array ; --$i ; ) {
		my $j = int rand( $i + 1 );
		next if $i == $j;
		@$array[ $i, $j ] = @$array[ $j, $i ];
	}
}

sub compute_diff
{    ##  $oldroot, $newroot  ##  just compute the difference between two pwms
	my ($old)    = $_[0];
	my ($new)    = $_[1];
	my ($diff)   = 0;
	my ($newval) = 0;
	my ($oldval) = 0;
	for ( $i = 0 ; $i < $site_len ; $i++ ) {
		foreach $nucleotide (@nts) {
			$oldval = $old->{PWM}[$i]->{$nucleotide};
			$newval = $new->{PWM}[$i]->{$nucleotide};
			$oldval = ( exp($oldval) - 0.01 ) * 0.25;
			$newval = ( exp($newval) - 0.01 ) * 0.25;

			
			$diff = $diff + $newval * log( $newval / $oldval );
		}

	}
	return $diff;

}
sub print_tree { 
	my ($treeroot)= $_[0];
	my ($len) = $_[1];
        return unless $treeroot;# }
	
	### print PWM
	print OUTTREE "len="."$len\n";
	for ( $j = 0 ; $j < $len ; $j++ ) {
                foreach $nucleotide (@nts) {
                                print OUTTREE "$treeroot->{PWM}[$j]->{$nucleotide}"."\t";
                        }
                        print OUTTREE "\n";
	}
	if (defined($treeroot->{POS})) {
		print OUTTREE "splitting point="."$treeroot->{POS}\n";} 
	else { 
		print OUTTREE "This is a leaf node\n"; return;}
	
	if (defined($treeroot->{TA})) { 
		print_tree($treeroot->{TA},$len-1); }
	else {print OUTTREE "A branch is null\n";}
	
	if (defined($treeroot->{TC})) { 
		print_tree($treeroot->{TC},$len-1); }
	else {print OUTTREE "C branch is null\n";}
	if (defined($treeroot->{TG})) { 
		print_tree($treeroot->{TG},$len-1); }
	else {print OUTTREE "G branch is null\n";}
	if (defined($treeroot->{TT})) { 
		print_tree($treeroot->{TT},$len-1); }
	else {print OUTTREE "T branch is null\n";}



}




sub compute_score {    # return the score of a sequence
	my ($treeroot) = $_[0];
	my ($seq)      = $_[1];

	
	my (@seq_array) = split //, $seq;
	my ($len) = scalar(@seq_array);

	
	my ($score) = 0;
	unless ( $treeroot->{POS} ) {

		
		for ( $ii = 0 ; $ii < $len ; $ii++ ) {

			
			$score += $treeroot->{PWM}[$ii]->{ $seq_array[$ii] };
		}

		return $score;    
	}
	my ($split_point) = $treeroot->{POS};
	my ($subseq)      = $seq;
	substr $subseq, $split_point, 1, "";

	
	my (@cons) = split //, $treeroot->{CON};
	if ( $seq_array[$split_point] eq 'a' ) 
	{
		if (defined($treeroot->{TA})) 
		{
			$score =
		  	$treeroot->{PWM}[$split_point]->{ $seq_array[$split_point] } +
		  	compute_score( $treeroot->{TA}, $subseq ); 
		}
		else 
		{
			for ( $ii = 0 ; $ii < $len ; $ii++ ) {

				$score += $treeroot->{PWM}[$ii]->{ $seq_array[$ii] };
                        }

		       return $score;
		}

	}
	elsif ( $seq_array[$split_point] eq 'c' ) {
		if (defined($treeroot->{TC})) 
		{
			$score =
		  	$treeroot->{PWM}[$split_point]->{ $seq_array[$split_point] } +
		  	compute_score( $treeroot->{TC}, $subseq ); 
		}
		else 
		{
			for ( $ii = 0 ; $ii < $len ; $ii++ ) {

				$score += $treeroot->{PWM}[$ii]->{ $seq_array[$ii] };
                        }

		       return $score;
		}
	}
	elsif ( $seq_array[$split_point] eq 'g' ) {
		if (defined($treeroot->{TG})) 
		{
			$score =
		  	$treeroot->{PWM}[$split_point]->{ $seq_array[$split_point] } +
		  	compute_score( $treeroot->{TG}, $subseq ); 
		}
		else 
		{
			for ( $ii = 0 ; $ii < $len ; $ii++ ) {

				$score += $treeroot->{PWM}[$ii]->{ $seq_array[$ii] };
                        }

		       return $score;
		}
	}
	elsif ( $seq_array[$split_point] eq 't' ) {
		if (defined($treeroot->{TT})) 
		{
			$score =
		  	$treeroot->{PWM}[$split_point]->{ $seq_array[$split_point] } +
		  	compute_score( $treeroot->{TT}, $subseq ); 
		}
		else 
		{
			for ( $ii = 0 ; $ii < $len ; $ii++ ) {

				$score += $treeroot->{PWM}[$ii]->{ $seq_array[$ii] };
                        }

		       return $score;
		}
	}
        else { print "compute_score"; die "$0: not acgt.\n";}

}

#find_bs( \@seq_list, $oldroot,\@bs_list, \@bs_list_st, \@bs_list_score );
sub find_bs {    #  should return a list,. not a file  \@seq_list, \$oldroot
	my (@seq_list)       = @{ $_[0] };
	my ($treeroot)       = $_[1];
	my ($length_seqlist) = scalar(@seq_list);

	
	

	for ( $i = 0 ; $i < $length_seqlist ; $i++ ) {
		my ($seq_len) = length( $seq_list[$i] );
		
		my ($max_score)   = -100;
		my ($max_st)      = 0;
		my ($total_score) = 0;
		for ( $st = 0 ; $st < $seq_len - $site_len + 1 ; ++$st ) {

			## array slice
			
			$subseq = substr $seq_list[$i], $st, $site_len;
			$total_score = compute_score( $treeroot, $subseq );
			if ( $total_score > $max_score ) {
				$max_score  = $total_score;
				$max_subseq = $subseq;
				$max_st     = $st;
			}

		}

		

		## push the bs into the bs_list
		if ( $max_score >= $cutoff ) {
			@{ $_[2] } = ( @{ $_[2] }, $max_subseq );
			@{ $_[3] } = ( @{ $_[3] }, $max_st );
			@{ $_[4] } = ( @{ $_[4] }, $max_score );
		}
	}

	

}


#find_bs( \@seq_list,\@rcseq_list, $oldroot,\@bs_list, \@bs_list_st, \@bs_list_score );
sub find_bs_double_strand {    #  should return a list,. not a file  \@seq_list, \$oldroot
	my (@seq_list)       = @{ $_[0] };
	my (@rcseq_list)       = @{ $_[1] };
	my ($treeroot)       = $_[2];
	my ($length_seqlist) = scalar(@seq_list);

	
	

	for ( $i = 0 ; $i < $length_seqlist ; $i++ ) {
		my ($seq_len) = length( $seq_list[$i] );
		
		my ($max_score)   = -1000;
		my ($max_st)      = 0;
		my ($total_score) = 0;
		for ( $st = 0 ; $st < $seq_len - $site_len + 1 ; ++$st ) {

			## array slice
			
			$subseq = substr $seq_list[$i], $st, $site_len;
			
			$total_score = compute_score( $treeroot, $subseq );
			if ( $total_score > $max_score ) {
				$max_score  = $total_score;
				$max_subseq = $subseq;
				$max_st     = $st;
			}

		}
		### for the reverse complement seq
		for ( $st = 0 ; $st < $seq_len - $site_len + 1 ; ++$st ) {

			## array slice
			
			$subseq = substr $rcseq_list[$i], $st, $site_len;
			
			$total_score = compute_score( $treeroot, $subseq );
			if ( $total_score > $max_score ) {
				$max_score  = $total_score;
				$max_subseq = $subseq;
				$max_st     = $st;
			}

		}
		########### end of the reverse complement
		

		## push the bs into the bs_list
		if ( $max_score >= $cutoff ) {
			@{ $_[3] } = ( @{ $_[3] }, $max_subseq );
			@{ $_[4] } = ( @{ $_[4] }, $max_st );
			@{ $_[5] } = ( @{ $_[5] }, $max_score );
		}
	}

	

}
###  return TRUE = 1 if it is a match, otherwise FALSE = 0
sub match_con {
	my ($nucltide) = $_[0];
	my ($con)      = $_[1];
	if ( $nucltide eq 'a' ) {
		if (   ( $con eq 'a' )
			or ( $con eq 'r' )
			or ( $con eq 'm' )
			or ( $con eq 'w' ) )
		{
			return 1;
		}
		else { return 0 }
	}
	elsif ( $nucltide eq 'c' ) {
		if (   ( $con eq 'c' )
			or ( $con eq 'y' )
			or ( $con eq 'm' )
			or ( $con eq 's' ) )
		{
			return 1;
		}
		else { return 0 }
	}
	elsif ( $nucltide eq 'g' ) {
		if (   ( $con eq 'g' )
			or ( $con eq 'r' )
			or ( $con eq 's' )
			or ( $con eq 'k' ) )
		{
			return 1;
		}
		else { return 0 }
	}
	elsif ( $nucltide eq 't' ) {
		if (   ( $con eq 't' )
			or ( $con eq 'y' )
			or ( $con eq 'w' )
			or ( $con eq 'k' ) )
		{
			return 1;
		}
		else { return 0 }
	}
	else { print "$nucltide\n"; die "$0: not acgt.\n"; }
}
##########construct_pwm( \@bs_list, $pwm, \$consensus );
sub construct_pwm {

	# input is a list of seqs
	my (@bs_listarray) = @{ $_[0] };
	my ($len_bs_list)  = scalar(@bs_listarray);
	$pwm     = $_[1];
	$conseq  = $_[2];
	$$conseq = "";
	print "$len_bs_list\n";
	### initializing the pwm
	foreach $nucleotide (@nts) {
		for ( $ii = 0 ; $ii < $site_len ; $ii++ ) {
			$pwm->[$ii]->{$nucleotide} = 0;
		}
	}
	
	for ( $kk = 0 ; $kk < $len_bs_list ; $kk++ ) {
		@sq = split //, $bs_listarray[$kk];

		
		for ( $ii = 0 ; $ii < $site_len ; $ii++ ) {
			$pwm->[$ii]->{ $sq[$ii] }++;
		}

	}

	

	for ( $j = 0 ; $j < $site_len ; $j++ ) {

		my ($max1) = -1000;
		my ($max2) = -1000;

		foreach $nucleotide (@nts) {

			
			$pwm->[$j]->{$nucleotide} =
			  log( $pwm->[$j]->{$nucleotide} / $len_bs_list / 0.25 + 0.01 );

			
			if ( $pwm->[$j]->{$nucleotide} >= $max1 ) {
				if ( $max1 > $max2 ) {
					$max2 = $max1;
					$cons[$j][1] = $cons[$j][0];
				}
				$max1 = $pwm->[$j]->{$nucleotide};
				$cons[$j][0] = $nucleotide;

			}
			elsif ( $pwm->[$j]->{$nucleotide} >= $max2 ) {
				$max2 = $pwm->[$j]->{$nucleotide};
				$cons[$j][1] = $nucleotide;
			}

		}

		
		if ( $cons[$j][0] eq 'a' ) {
			if    ( $cons[$j][1] eq 'c' ) { $$conseq = $$conseq . "m" }
			elsif ( $cons[$j][1] eq 'g' ) { $$conseq = $$conseq . "r" }
			else                          { $$conseq = $$conseq . "w" }
		}
		elsif ( $cons[$j][0] eq 'c' ) {
			if    ( $cons[$j][1] eq 'a' ) { $$conseq = $$conseq . "m" }
			elsif ( $cons[$j][1] eq 'g' ) { $$conseq = $$conseq . "s" }
			else                          { $$conseq = $$conseq . "y" }
		}
		elsif ( $cons[$j][0] eq 'g' ) {
			if    ( $cons[$j][1] eq 'a' ) { $$conseq = $$conseq . "r" }
			elsif ( $cons[$j][1] eq 'c' ) { $$conseq = $$conseq . "s" }
			else                          { $$conseq = $$conseq . "k" }
		}
		else {
			if    ( $cons[$j][1] eq 'a' ) { $$conseq = $$conseq . "w" }
			elsif ( $cons[$j][1] eq 'c' ) { $$conseq = $$conseq . "y" }
			else                          { $$conseq = $$conseq . "k" }
		}
	}


}

sub max {
	my (@array)     = @{ $_[0] };
	my ($larray)    = $_[1];
	my ($max_value) = 0;
	my ($max_index) = 0;

	
	for ( $index = 0 ; $index < $larray ; $index++ ) {
		if ( $array[$index] > $max_value ) {
			$max_value = $array[$index];
			$max_index = $index;
		}

	}
	return ($max_index);
}

sub min {
	my (@array)     = @{ $_[0] };
	my ($larray)    = $_[1];
	my ($min_value) = 100000;
	my ($min_index) = 0;

	
	for ( $index = 0 ; $index < $larray ; $index++ ) {
		if ( $array[$index] < $min_value ) {
			$min_value = $array[$index];
			$min_index = $index;
		}

	}
	return ($min_index);
}


sub construct_dpwm {    #  contruct the dpwm tree
	                    #my ( @bs_list, @pwm, $consensus , $root) = @_;
	my (@bs_list_inside) = @{ $_[0] };
	my (@subpwm)         = @{ $_[1] };

	my ($subconsensus) = $_[2];
	my ($tree)         = $_[3];
	my ($llist)        = scalar(@bs_list_inside);
	my ($lmotif)       = length($subconsensus);
	print "length of motif = $lmotif\t" . "number of bs = $llist\n";
	my (@subconarray) = split //, $subconsensus;
	unless ( $lmotif > 1 ) {

		
	
		$tree->{PWM} = [@subpwm];
		$tree->{TA}  = undef;
		$tree->{TC}  = undef;
		$tree->{TG}  = undef;
		$tree->{TT}  = undef;
		$tree->{POS} = undef;
		$tree->{CON} = $subconsensus;
		$_[3]        = $tree;          
		print "the height of the tree is l-1 now\n";
		return;
	}

	my (@abs_list) = ();
	my (@cbs_list) = ();
	my (@gbs_list) = ();
	my (@tbs_list) = ();
	my (@apwm)     = ();
	my (@cpwm)     = ();
	my (@gpwm)     = ();
	my (@tpwm)     = ();
	my (@sumchi)   = ();
	for ( $i = 0 ; $i < $lmotif ; $i++ )
	{    # store the hamming distance fro each position
		$sumchi[$i] = 0;
	}
	my ($nbs) = scalar(@bs_list_inside);

	

	#################################################
	my (@freq)      = ();
	my (@chisquare) = ();
	my (@hd)        = ();
	### initialize
	for ( $i = 0 ; $i < $lmotif ; $i++ ) {
		$freq[$i][0] = 0;
		$freq[$i][1] = 0;
		$freq[$i][2] = 0;
		$freq[$i][3] = 0;
		for ( $j = 0 ; $j < $lmotif ; $j++ ) {
			$chisquare[$i][$j][0][0] = 0;
			$chisquare[$i][$j][0][1] = 0;
			$chisquare[$i][$j][0][2] = 0;
			$chisquare[$i][$j][0][3] = 0;
			$chisquare[$i][$j][1][0] = 0;
			$chisquare[$i][$j][1][1] = 0;
			$chisquare[$i][$j][1][2] = 0;
			$chisquare[$i][$j][1][3] = 0;
			$chisquare[$i][$j][2][0] = 0;
			$chisquare[$i][$j][2][1] = 0;
			$chisquare[$i][$j][2][2] = 0;
			$chisquare[$i][$j][2][3] = 0;
			$chisquare[$i][$j][3][0] = 0;
			$chisquare[$i][$j][3][1] = 0;
			$chisquare[$i][$j][3][2] = 0;
			$chisquare[$i][$j][3][3] = 0;
		}
	}

	###### end of initialize
	### count ACGT
	foreach $bs (@bs_list_inside) {
		@bs = split //, $bs;
		for ( $i = 0 ; $i < $lmotif ; $i++ ) {
			if ( $bs[$i] eq 'a' ) {
				$freq[$i][0]++;
				for ( $j = $i + 1 ; $j < $lmotif ; $j++ ) {
					if ( $bs[$j] eq 'a' ) {
						$chisquare[$i][$j][0][0]++;
					}
					elsif ( $bs[$j] eq 'c' ) {
						$chisquare[$i][$j][0][1]++;
					}
					elsif ( $bs[$j] eq 'g' ) {
						$chisquare[$i][$j][0][2]++;
					}
					else { $chisquare[$i][$j][0][3]++; }
				}
			}
			elsif ( $bs[$i] eq 'c' ) {

				$freq[$i][1]++;
				for ( $j = $i + 1 ; $j < $lmotif ; $j++ ) {
					if ( $bs[$j] eq 'a' ) {
						$chisquare[$i][$j][1][0]++;
					}
					elsif ( $bs[$j] eq 'c' ) {
						$chisquare[$i][$j][1][1]++;
					}
					elsif ( $bs[$j] eq 'g' ) {
						$chisquare[$i][$j][1][2]++;
					}
					else { $chisquare[$i][$j][1][3]++; }
				}
			}
			elsif ( $bs[$i] eq 'g' ) {

				$freq[$i][2]++;
				for ( $j = $i + 1 ; $j < $lmotif ; $j++ ) {
					if ( $bs[$j] eq 'a' ) {
						$chisquare[$i][$j][2][0]++;
					}
					elsif ( $bs[$j] eq 'c' ) {
						$chisquare[$i][$j][2][1]++;
					}
					elsif ( $bs[$j] eq 'g' ) {
						$chisquare[$i][$j][2][2]++;
					}
					else { $chisquare[$i][$j][2][3]++; }
				}
			}
			else {

				$freq[$i][3]++;
				for ( $j = $i + 1 ; $j < $lmotif ; $j++ ) {
					if ( $bs[$j] eq 'a' ) {
						$chisquare[$i][$j][3][0]++;
					}
					elsif ( $bs[$j] eq 'c' ) {
						$chisquare[$i][$j][3][1]++;
					}
					elsif ( $bs[$j] eq 'g' ) {
						$chisquare[$i][$j][3][2]++;
					}
					else { $chisquare[$i][$j][3][3]++; }
				}
			}

		}
	}
	## compute the Hamming distance for Ni vs Nj
	for ( $i = 0 ; $i < $lmotif ; $i++ ) {
		for ( $j = 0 ; $j < $lmotif ; $j++ ) {
			$hd[$i][$j] = 0;
		}
	}
	my ($max_hd) = 0.2;
	for ( $i = 0 ; $i < $lmotif ; $i++ ) {
		for ( $j = $i + 1 ; $j < $lmotif ; $j++ ) {

			for ( $ii = 0 ; $ii < 4 ; $ii++ ) {
				for ( $jj = 0 ; $jj < 4 ; $jj++ ) {
					$hd[$i][$j] =
					  $hd[$i][$j] +
					  abs( $chisquare[$i][$j][$ii][$jj] / $nbs -
						  $freq[$i][$ii] * $freq[$j][$jj] / $nbs / $nbs );
				}
			}
			if ( $hd[$i][$j] > $max_hd ) { $max_hd = $hd[$i][$j] }
			$hd[$j][$i] = $hd[$i][$j];
		}
	}

	## compute the sum of HD for each position

	for ( $i = 0 ; $i < $lmotif ; $i++ ) {
		for ( $j = 0 ; $j < $lmotif ; $j++ ) {

			$sumchi[$i] = $sumchi[$i] + $hd[$i][$j];

		}

		
	}

	if ( $max_hd <= 0.2 ) {
		$tree->{PWM} = [@subpwm];
		$tree->{TA}  = undef;
		$tree->{TC}  = undef;
		$tree->{TG}  = undef;
		$tree->{TT}  = undef;
		$tree->{POS} = undef;
		$tree->{CON} = $subconsensus;
		$_[3]        = $tree;
		print "the overall max_hd=" . "$max_hd\n";
		return;
	}
	############  searching for positon max_pos,(at least one pair is larger than 0.2)
	my ($max_pos) = 0;

	$max_hd = 0.2;
	$max_pos = max( \@sumchi, $lmotif );
	for ( $j = 0 ; $j < $lmotif ; $j++ ) {
		if ( $hd[$max_pos][$j] > $max_hd ) { $max_hd = $hd[$max_pos][$j] }
	}
	while ( $max_hd <= 0.2 ) {

		$sumchi[$max_pos] = 0;
		$max_pos = max( \@sumchi, $lmotif );
		for ( $j = 0 ; $j < $lmotif ; $j++ ) {
			if ( $hd[$max_pos][$j] > $max_hd ) {
				$max_hd = $hd[$max_pos][$j];
			}
		}
	}

	print "max position = " . "$max_pos\n";
	#########################################
	                                   
	$tree->{PWM} = [@subpwm];
	$tree->{TA}  = {};
	$tree->{TC}  = {};
	$tree->{TG}  = {};
	$tree->{TT}  = {};
	$tree->{POS} = undef;#$max_pos;
	$tree->{CON} = $subconsensus;
	$_[3]        = $tree;

	#########  construct left bs and right bs
	# initializing the lpwm and rpwm
	foreach $nucleotide (@nts) {
		for ( $ii = 0 ; $ii < $lmotif - 1 ; $ii++ ) {
			$apwm[$ii]->{$nucleotide} = 0;
			$cpwm[$ii]->{$nucleotide} = 0;
			$gpwm[$ii]->{$nucleotide} = 0;
			$tpwm[$ii]->{$nucleotide} = 0;
		}
	}
	foreach $bs (@bs_list_inside) {

		
		@bsarray = split //, $bs;
		if ( $bsarray[$max_pos] eq 'a' ) {
			substr $bs, $max_pos, 1,
			  "";    ## delete the nucletide at position $max_pos
			        
			@bsarray = split //, $bs;
			@abs_list = ( @abs_list, $bs );
			for ( $k = 0 ; $k < ( $lmotif - 1 ) ; $k++ ) {
				$apwm[$k]->{ $bsarray[$k] }++;

				
			}
		}
		elsif ( $bsarray[$max_pos] eq 'c' ) {
			substr $bs, $max_pos, 1,
			  "";    ## delete the nucletide at position $max_pos
			@bsarray = split //, $bs;
			@cbs_list = ( @cbs_list, $bs );
			for ( $k = 0 ; $k < ( $lmotif - 1 ) ; $k++ ) {
				$cpwm[$k]->{ $bsarray[$k] }++;

				
			}
		}
		elsif ( $bsarray[$max_pos] eq 'g' ) {
			substr $bs, $max_pos, 1,
			  "";    ## delete the nucletide at position $max_pos
			@bsarray = split //, $bs;
			@gbs_list = ( @gbs_list, $bs );
			for ( $k = 0 ; $k < ( $lmotif - 1 ) ; $k++ ) {
				$gpwm[$k]->{ $bsarray[$k] }++;

			}
		}
		elsif ( $bsarray[$max_pos] eq 't' ) {
			substr $bs, $max_pos, 1,
			  "";    ## delete the nucletide at position $max_pos
			@bsarray = split //, $bs;
			@tbs_list = ( @tbs_list, $bs );
			for ( $k = 0 ; $k < ( $lmotif - 1 ) ; $k++ ) {
				$tpwm[$k]->{ $bsarray[$k] }++;

			}
		}
	}
	### change it to frequency
	my ($nabs) = scalar(@abs_list);
	my ($ncbs) = scalar(@cbs_list);
	my ($ngbs) = scalar(@gbs_list);
	my ($ntbs) = scalar(@tbs_list);
	substr $subconsensus, $max_pos, 1, "";
###  A branch
	if ( $nabs >= $stop_number ) {
		foreach $nucleotide (@nts) {
			for ( $j = 0 ; $j < $lmotif - 1 ; $j++ ) {
				$apwm[$j]->{$nucleotide} =
				  log( $apwm[$j]->{$nucleotide} / $nabs / 0.25 + 0.01 );
			}
		}
		$tree->{POS} = $max_pos;
		construct_dpwm( \@abs_list, \@apwm, $subconsensus, $tree->{TA} );
	}
	else {
		$tree->{TA}  = undef;
		print "nabs is less than $stop_number\t" . "nabs="
		  . "$nabs\n";
	}

###  C branch
	if ( $ncbs >= $stop_number ) {
		foreach $nucleotide (@nts) {
			for ( $j = 0 ; $j < $lmotif - 1 ; $j++ ) {
				$cpwm[$j]->{$nucleotide} =
				  log( $cpwm[$j]->{$nucleotide} / $ncbs / 0.25 + 0.01 );
			}
		}
		$tree->{POS} = $max_pos;
		construct_dpwm( \@cbs_list, \@cpwm, $subconsensus, $tree->{TC} );
	}
	else {
		$tree->{TC}  = undef;
		print "ncbs is less than $stop_number\t" . "ncbs="
		  . "$ncbs\n";
	}

###  G branch
	if ( $ngbs >= $stop_number ) {
		foreach $nucleotide (@nts) {
			for ( $j = 0 ; $j < $lmotif - 1 ; $j++ ) {
				$gpwm[$j]->{$nucleotide} =
				  log( $gpwm[$j]->{$nucleotide} / $ngbs / 0.25 + 0.01 );
			}
		}
		$tree->{POS} = $max_pos;
		construct_dpwm( \@gbs_list, \@gpwm, $subconsensus, $tree->{TG} );
	}
	else {
		$tree->{TG}  = undef;
		print "ngbs is less than $stop_number\t" . "ngbs="
		  . "$ngbs\n";
	}

###  T branch
	if ( $ntbs >= $stop_number ) {
		foreach $nucleotide (@nts) {
			for ( $j = 0 ; $j < $lmotif - 1 ; $j++ ) {
				$tpwm[$j]->{$nucleotide} =
				  log( $tpwm[$j]->{$nucleotide} / $ntbs / 0.25 + 0.01 );
			}
		}
		$tree->{POS} = $max_pos;
		construct_dpwm( \@tbs_list, \@tpwm, $subconsensus, $tree->{TT} );
	}
	else {
		$tree->{TT}  = undef;
		print "ntbs is less than $stop_number\t" . "ntbs="
		  . "$ntbs\n";
	}


}

### START
&print_current_time;
$numArgs = $#ARGV + 1;
if ($numArgs != 11)  {
die "usage:perl TPD.pl s_plus_fasta_file(1) s_negative_fasta_file(2) initial_pwm_file(3) site_length(4) single_or_double_strand(5) output_treepwm_file(6) leftFPR(7) rightFPR(8) step(9) inputforscan(10) outputbed(11)" }

$inputSplus = "$ARGV[0]";    #input fasta file for S+

$inputSnegative = "$ARGV[1]";    #input fasta file for S-

$iniFile = "$ARGV[2]"; 

$site_len = "$ARGV[3]";


$bpwm = 1;
$strand = "$ARGV[4]"; ##  if eq 1 then single strand, else if 2 then double strand
$outputTree     = "$ARGV[5]";   #  output file for tree PWM


$leftFPR     = "$ARGV[6]";
$rightFPR     = "$ARGV[7]";
$step = "$ARGV[8]";
$inputforscan =  "$ARGV[9]";
$outputbed =  "$ARGV[10]";

open( INSP, $inputSplus )
  || die "$0: Couldn't open file $inputSplus for reading.\n";

open( INSN, $inputSnegative )
  || die "$0: Couldn't open file $inputSnegative for reading.\n";

open (IN_1, $iniFile)
    ||  die "$0: Couldn't open file $iniFile for reading.\n";

open( OUTTREE, ">$outputTree" )
  || die "$0: Couldn't open file $outputTree for writing.\n";




$/           = "\n";
@nts         = ( 'a', 'c', 'g', 't' );

$stop_number = 50;
### the follwong codes construct the PWM and $newroot based on input binding sites list (it's an array of hash)
$newroot    = {};
$pwm       = [];
$consensus = "";

$scan_count = 0;


if ($bpwm)  {

foreach $nucleotide (@nts) {
		for ( $ii = 0 ; $ii < $site_len ; $ii++ ) {
			$pwm->[$ii]->{$nucleotide} = 0;
		}
	}
	

while (<IN_1>) {
	chomp($_);
	
	@sq = split /\s+/, $_;

$j =1;
foreach $nucleotide (@nts) {
$pwm->[$scan_count]->{$nucleotide} = $sq[$j]; $j++; }
$consensus = $consensus.$sq[5];
++$scan_count;
}

print "$scan_count\n";
print "$site_len\n";
print "$consensus\n";


$lenbslist = 0;
foreach $nucleotide (@nts) {
$lenbslist = $lenbslist + $pwm->[0]->{$nucleotide} ; }
print "$lenbslist\n";
for ( $j = 0 ; $j < $site_len ; $j++ ) {
foreach $nucleotide (@nts) {

			#print "$pwm->[$j]->{$nucleotide}\n";
			$pwm->[$j]->{$nucleotide} =
			  log( $pwm->[$j]->{$nucleotide} / $lenbslist / 0.25 + 0.01 );
}
}

$consensus =~ tr/ACGTN/acgtn/;
$newroot->{PWM}   = $pwm;
		$newroot->{TA}  = undef;
		$newroot->{TC} = undef;
		$newroot->{TG} = undef;
		$newroot->{TT} = undef;
		$newroot->{POS}   = undef;
		$newroot->{CON}   = $consensus;


} else {
########### if $bpwm = 0

while (<IN_1>) {
	chomp($_);
	$_ =~ tr/ACGTN/acgtn/;
	@sq = split / /, $_;
	#s/ //;
	
	$bs_list[$scan_count] = $sq[0];
#	print "$sq[0]\n";
	#print "$_";
	++$scan_count;
	$bs_list[$scan_count] = $sq[1];
#	print "$sq[1]\n";
	++$scan_count;
}
print "bpwm=0\n";
print "$scan_count\n";


construct_pwm( \@bs_list, $pwm, \$consensus );

print "$consensus\n";

construct_dpwm( \@bs_list, $pwm, $consensus, $newroot );


################# end  if $bpwm = 0
}

$iniroot = $newroot;


###all seqs must be tr/A-Z/a-z/; # Canonicalize to lower case.
############ read the positive seqs from file into memory

####   for seqs with "n"s, the code seperates the seqs into several segments according to the position of the "n"s 
$scan_count = 0;

$total_count_p = 0;
$/           = "\n>";
$output_count = 0;

while (<INSP>) {

	

	# separate the lines in the input record
	( $header, @linz ) = split /\n/;

	$seqlinz = join '', grep !/>/, @linz;
	$seqlinz =~ s/\s+//g;
	$header  =~ s/\>//;
	$seqlinz =~ tr/ACGTN/acgtn/;
	$seqlinz =~ s/\>//;    ### chop the last >
	
	###########   seqs with ns are all ignored
	
	if ( not( $seqlinz =~ /n{1,800}/gi ) ) {
		$output_count ++;
		
		$seq_index[$total_count_p] = 1;
		#print "$lengthlinz\n";
		@seq_list  = ( @seq_list,  $seqlinz );
		@head_list = ( @head_list, $header );
	}    else {
		@heads = split(/\:/,$header);
		@tmp = split(/\-/,$heads[1]);	     	
		#@head_list = ( @head_list, $header );
		#$NePosition = pos($seqlinz)-1;
		$segstart = 0;
		$seglength = $-[0] - $segstart ;
		########### all segs with $seglength < $site_len are ignored..
		
		print "There are Ns in this seq\t"."$header\n";
		print "$seglength\t";
		if ( $seglength >=$site_len  ) {
			$seg = substr $seqlinz, $segstart, $seglength;			
			@seq_list  = ( @seq_list,  $seg );
			$end = $tmp[0]+$seglength-1;
			$header = "$heads[0]".":"."$tmp[0]"."-"."$end";
			print "$header\n";
			@head_list = ( @head_list, $header);
			++$scan_count;
		}
		$segstart = pos($seqlinz);		
		while ($seqlinz =~ /n{1,800}/gi) {

			$seglength = $-[0] - $segstart ;
			print "$seglength\t";			
			#$NePosition = pos($seqlinz)-1;
			if ( $seglength >=$site_len  ) {
				$seg = substr $seqlinz, $segstart, $seglength;				
				@seq_list  = ( @seq_list,  $seg );
				$start = $tmp[0] + $segstart;
				$end =$start+$seglength-1;
				 $header = "$heads[0]".":"."$start"."-"."$end";
				print "$header\n";
                      		 @head_list = ( @head_list, $header);
				++$scan_count;
			}
			$segstart = pos($seqlinz);		
			
		}		
		$seqlen = length($seqlinz);
		$seglength = $seqlen - $segstart;
		print "$seglength\t";		
		if ( $seglength >=$site_len  ) {
				$seg = substr $seqlinz, $segstart, $seglength;				
				@seq_list  = ( @seq_list,  $seg );
				$start = $tmp[0] + $segstart;
                                $end =$start+$seglength-1;
                                 $header = "$heads[0]".":"."$start"."-"."$end";
				print "$header\n";
				@head_list = (@head_list, $header);
				++$scan_count;
			}		
		$seq_index[$total_count_p] = $scan_count;
		$output_count = $output_count + $scan_count;
		$scan_count = 0;
		
	     }
	   $total_count_p++;
}

print "positive seq:\t" . "$total_count_p\t" . "$output_count\n";

### generate the reverse complement
if ($strand == 2) {
print "DOUBLE STRAND SCANNED for positive set\n";

for ($i=0; $i< $output_count; $i++) {
	$rcseq = reverseComplement($seq_list[$i]);
	@rcseq_list  = ( @rcseq_list,  $rcseq );

}
}
############ read the negative seqs from file into memory

$scan_count = 0;

$total_count = 0;
$/           = "\n>";

while (<INSN>) {

	$total_count++;

	# separate the lines in the input record
	( $header, @linz ) = split /\n/;

	$seqlinz = join '', grep !/>/, @linz;
	$seqlinz =~ s/\s+//g;
	$header  =~ s/\>//;
	$seqlinz =~ tr/ACGTN/acgtn/;

	
	###########   seqs with ns are all ignored
	if ( not( $seqlinz =~ /n/ ) ) {
		++$scan_count;
		$seqlinz =~ s/\>//;    

		
		@seqn_array = split //, $seqlinz;
		
		$seqlinz = join '', @seqn_array;
		@seq_list_negative  = ( @seq_list_negative,  $seqlinz );
		@head_list_negative = ( @head_list_negative, $header );
	}    
}
### generate the reverse complement
if ($strand == 2) {
print "DOUBLE STRAND SCANNED for negative set\n";

for ($i=0; $i< $scan_count; $i++) {
	$rcseq = reverseComplement($seq_list_negative[$i]);
	@rcseq_list_negative  = ( @rcseq_list_negative,  $rcseq );

}
}
print "negative seq:\t" . "$total_count\t" . "$scan_count\n";

#############################################

$stop_number = 200;
$cutoff      = -1000;  ## this number must be less than -100 in the find_bs procedure
$true_cutoff = 0;

############### the begining of the iteration
$smallvalue = 0.001;
$diff       = 10;
$iterative  = 0;


### new added code for best mcc
$fp_rate = $leftFPR;
#$step = 0.01;
$oldmcc = -1;
$maxmcc = -1;
$newmcc = 0;
$maxfp_rate = 0.1;

while ($fp_rate <= $rightFPR) {

print "*********************** FPR="."$fp_rate\n";
## end new added

$newroot = $iniroot;  # for tanseither
while ( $diff > $smallvalue and $iterative < 15 ) {
### the following codes
	$iterative++;
	$oldroot = $newroot;

	$newroot = {};    ## assign a new hash reference to $newroot

### determine the cutoff value based on the $fp_rate and \@bs_list_score_n
	@bs_list_n       = ();
	@bs_list_st_n    = ();
	@bs_list_score_n = ();

	$cutoff = -1000;    ### $cutoff is globla
if ($strand == 2) {find_bs_double_strand( \@seq_list_negative, \@rcseq_list_negative, $oldroot, \@bs_list_n, \@bs_list_st_n,
		\@bs_list_score_n );} 
else	{ find_bs( \@seq_list_negative, $oldroot, \@bs_list_n, \@bs_list_st_n,
		\@bs_list_score_n );}



	@bs_list_score_n = sort { $b <=> $a } @bs_list_score_n;
	$index_cutoff    = int( $scan_count * $fp_rate );
	$true_cutoff          = $bs_list_score_n[$index_cutoff];
	print "true_cutoff="."$true_cutoff\n";

	@bs_list_p       = ();
	@bs_list_st_p    = ();
	@bs_list_score_p = ();

if ($strand == 2) { find_bs_double_strand( \@seq_list,\@rcseq_list, $oldroot, \@bs_list_p, \@bs_list_st_p,
		\@bs_list_score_p );}
else {	
find_bs( \@seq_list, $oldroot, \@bs_list_p, \@bs_list_st_p,
		\@bs_list_score_p );}

	
	@bs_list_p2       = ();
	@bs_list_st_p2    = ();
	@bs_list_score_p2 = ();
	## $output_count should be equal to the size of @bs_list_score_n : sort descending
### merge segs coming from the same seq based on @seq_index
## $bs_list_score_p2  @bs_list_st_p2  @bs_list_p2
    $index_output = 0;  
	for ($i = 0; $i < $total_count_p; $i++) {
		if ($seq_index[$i] < 1) { ## means no bs found in this seq
		$temp = $bs_list_score_p[$index_output];		
		print "the index is zero and i ="."$i\t"."$temp\n";
		
                }			
		if ($seq_index[$i] == 1) {
		  $bs_list_score_p2[$i] = $bs_list_score_p[$index_output];
		  $bs_list_st_p2[$i] = $bs_list_st_p[$index_output];
		 $bs_list_p2[$i] = $bs_list_p[$index_output];
		$index_output++;
						
		}  else {
			@sub = @bs_list_score_p[$index_output..($index_output + $seq_index[$i]-1)];
			$offset = max(\@sub,$seq_index[$i] ) ;			
			$bs_list_score_p2[$i] = $bs_list_score_p[$index_output + $offset];
		  	$bs_list_st_p2[$i] = $bs_list_st_p[$index_output + $offset];
		 	$bs_list_p2[$i] = $bs_list_p[$index_output + $offset];
			$index_output = $index_output + $seq_index[$i];	
		}
	}
	@bs_list_p3 = ();
	for ($i = 0; $i < $total_count_p; $i++) {
		if ( $bs_list_score_p2[$i] >= $true_cutoff ) {
			@bs_list_p3 = (@bs_list_p3, $bs_list_p2[$i]);		
		}
	}
$bs_tp = @bs_list_p3;
print "index_output="."$index_output\n";
print "iteration number="."$iterative\t"."number of bs="."$bs_tp\t" . "$true_cutoff\n";
### construct the consensus based on the returned list : IUPAC
##  consider $consensus as a global variable
###  construct the pwm
	$pwm = [];

	construct_pwm( \@bs_list_p3, $pwm, \$consensus );





	
	print "$consensus\n";

	

	construct_dpwm( \@bs_list_p3, $pwm, $consensus, $newroot );

############### compute the difference between the two pwms
	$diff = compute_diff( $oldroot, $newroot );
	print "diff=" . "$diff\n";

}

### new added code for best mcc

$tp = $bs_tp/$index_output;
$fn = 1- $tp;
$tn = 1-$fp_rate; 
$fp = $fp_rate;




$oldmcc = $newmcc;
$newmcc = $tp*$tn - $fp*$fn;
$newmcc = $newmcc / sqrt(($fp+$tp)*($tp+$fn)*($tn+$fn)*($tn+$fp));
print "**************************\n";
print "$fp_rate\t"."$tp\t"."$newmcc\n";

if ($newmcc > $maxmcc and $iterative < 15 )   {  ## 
	$maxfp_rate = $fp_rate;
	$maxmcc = $newmcc;
	$maxroot = $newroot;
	$maxcutoff = $true_cutoff;
	}
$diff =10;
$iterative  = 0;
$fp_rate = $fp_rate + $step;

}

########## end added code for best mcc
$fp_rate = $maxfp_rate;
print "best MCC is achieved at FPR = $fp_rate\n";


################### print pwm
print_tree($maxroot,$site_len);



&print_current_time;

close(INSP);
close(INSN);
close(IN_1);
close(OUTTREE);

 ############################ new added for scan new seqs


######### store
#use Storable qw(store retrieve freeze thaw dclone);
#store($maxroot, '/tmp/maxroot_HET') or die "Can't store $maxroot in /tmp/maxroot_HE!\n";

#####
open( OUTBED, ">$outputbed" )
  || die "$0: Couldn't open file $outputbed for writing.\n";


open( INSCAN, $inputforscan )
  || die "$0: Couldn't open file $inputforscan for reading.\n";

$scan_count = 0;

$total_count = 0;
$/           = "\n>";
@seq_list_negative = ();
@head_list_negative = ();
while (<INSCAN>) {

	$total_count++;

	# separate the lines in the input record
	( $header, @linz ) = split /\n/;

	$seqlinz = join '', grep !/>/, @linz;
	$seqlinz =~ s/\s+//g;
	$header  =~ s/\>//;
	$seqlinz =~ tr/ACGTN/acgtn/;

	#$lengthlinz = length ($seqlinz);
	###########   seqs with ns are all ignored
	if ( not( $seqlinz =~ /n/ ) ) {
		++$scan_count;
		$seqlinz =~ s/\>//;    ### chop the last >

		#print "$lengthlinz\n";
		@seqn_array = split //, $seqlinz;
		#fisher_yates_shuffle( \@seqn_array );
		$seqlinz = join '', @seqn_array;
		@seq_list_negative  = ( @seq_list_negative,  $seqlinz );
		@head_list_negative = ( @head_list_negative, $header );
	}    
}
### generate the reverse complement
if ($strand == 2) {
print "DOUBLE STRAND SCANNED for new seqs\n";
@rcseq_list_negative = ();
for ($i=0; $i< $scan_count; $i++) {
	$rcseq = reverseComplement($seq_list_negative[$i]);
	@rcseq_list_negative  = ( @rcseq_list_negative,  $rcseq );

}
}
print "new seqs:\t" . "$total_count\t" . "$scan_count\n";


$cutoff = $maxcutoff;    ### $cutoff is globla
print "cutoff="."$cutoff\n";


if ($strand == 2) { find_multiple_bs_double_strand2( \@seq_list_negative,\@rcseq_list_negative, $maxroot, $cutoff );}

close(INSCAN);
close(OUTBED);

